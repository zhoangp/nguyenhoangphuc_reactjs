import logo from './logo.png';
import './App.css';
import Header from './Components/header.jsx';
import Slider from './Components/slider.jsx';
import SmartPhone from './Components/smartphone.jsx';
import Laptop from './Components/Laptop.jsx';
import Promotion from './Components/Promotion.jsx';
function App() {
  return (
    <div className="App">
      <Header></Header>
      <Slider></Slider>
      <SmartPhone></SmartPhone>
      <Laptop></Laptop>
      <Promotion></Promotion>
    </div>
  );
}

export default App;
